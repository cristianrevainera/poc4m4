package com.example.skip.poc4.config;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * Application configuration
 */
@SpringBootApplication
public class Application {

    /**
     * Rest template bean
     *
     * @param builder {@link RestTemplateBuilder} builter
     * @return rest template
     */
    @Bean
    public RestTemplate restTemplate(final RestTemplateBuilder builder) {
        return builder.build();
    }
}
