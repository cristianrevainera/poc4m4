package com.example.skip.poc4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Prof of concept application
 */
@SpringBootApplication
public class Poc4m4Application {

/**
 * Root method
 * @param args command line parameters
 */
public static void main(final String[] args) {
SpringApplication.run(Poc4m4Application.class, args);
}
}
