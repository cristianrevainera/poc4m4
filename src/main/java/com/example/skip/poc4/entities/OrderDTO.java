package com.example.skip.poc4.entities;


//import lombok.Getter;
//import lombok.NoArgsConstructor;
//import lombok.Setter;

/**
 * Order entity DTO
 */

public class OrderDTO {
    private String email;

    /**
     * Retrieves the order email
     * @return (@link String) the order email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Changes an order email
     * @param email (@link String) changes an order email
     */
    public void setEmail(final String email) {
        this.email = email;
    }
}
