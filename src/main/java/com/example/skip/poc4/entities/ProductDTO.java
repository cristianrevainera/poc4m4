package com.example.skip.poc4.entities;


//import lombok.Getter;
//import lombok.NoArgsConstructor;
//import lombok.Setter;
//@Getter
//@Setter
//@NoArgsConstructor

/**
 * Product entity DTO
 */
public class ProductDTO {
    private String name;
    private OrderDTO order;

    /**
     * Retrieves a name
     * @return (@link String) sproduct name
     */
    public String getName() {
        return name;
    }

    /**
     * Change the product name
     * @param name (@link String) product name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Chage the order
     * @return (@link Oderder) order to be retrieved
     */
    public OrderDTO getOrder() {
        return order;
    }

    /**
     * Retrieves an order to be changed
     * @param order (@link order) order to be changed
     */
    public void setOrder(final OrderDTO order) {
        this.order = order;
    }
}
