package com.example.skip.poc4.exceptions;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Una Wrapper para los errores pasa de String a Object.
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class ErrorWrapper {
    private String message;
}
