package com.example.skip.poc4.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception creada especificamente para el manejo de nuestras exceptions.
 */
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class DBException extends Exception {
    /**
     *
     * @param s {@link String}
     */
    public DBException(final String s) {
        super(s);
    }

    /**
     *
     * @param s {@link String}
     * @param throwable {@link Throwable}
     */
    public DBException(final String s, final Throwable throwable) {
        super(s, throwable);
    }
}
