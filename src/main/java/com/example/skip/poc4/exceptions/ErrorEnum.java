package com.example.skip.poc4.exceptions;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Enum con los errores
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum ErrorEnum {
    JSON_MARSHALLING_ERROR("Error haciendo marshalling del Json."),
    JMS_ERROR("Java message service error."),
    REGISTRY_NOT_FOUND_ERROR("Registry not found error");

    private String msg;

}
