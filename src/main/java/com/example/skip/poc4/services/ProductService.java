package com.example.skip.poc4.services;

import com.example.skip.poc4.entities.ProductDTO;
import com.example.skip.poc4.exceptions.DBException;

import java.util.List;

/**
 * ProductDTO service layer definition
 */
public interface ProductService {

    /**
     * Retrieves a ProductDTO with the given id
     * @param id (@Link Long) ProductDTO's identification number
     * @return (@Link ProductDTO) the found ProductDTO
     * @throws DBException An exception if the ProductDTO doesn't exist in the database
     */
    ProductDTO getById(Long id) throws DBException;

    /**
     * Retrieves all the ProductDTOs
     * @return (@Link ProductDTO) a list of ProductDTOs
     */
    List<ProductDTO> getAll();
}
