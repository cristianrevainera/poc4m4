package com.example.skip.poc4.services;

import com.example.skip.poc4.entities.ProductDTO;
import com.example.skip.poc4.exceptions.DBException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Product Service layer implementation
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Value("${poc4m1.url}")
    private String poc4m1Url;

    private RestTemplate restTemplate;

    /**
     * Product service constructor
     * @param restTemplate {@link RestTemplate} rest template
     */
    @Autowired
    public ProductServiceImpl(final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    /**
     * Retrieves a product by an identification number
     * @param id (@link Long) ProductDTO's identification number
     * @return ProductDTO {@link ProductDTO} return a product
     * @throws DBException
     */
    @Override
    public ProductDTO getById(final Long id) throws DBException {
        return restTemplate.getForObject(poc4m1Url + "/products/{id}", ProductDTO.class, id);
    }

    /**
     * Retrieves all products list
     * @return ProductDTO {@link ProductDTO} return a product list
     */
    @Override
    public List<ProductDTO> getAll() {

//        ParameterizedTypeReference<List<ProductDTO>> parameterizedTypeReference =
//                new ParameterizedTypeReference<List<ProductDTO>>() { };
//
//
        ResponseEntity<List<ProductDTO>> response = restTemplate.exchange(
                poc4m1Url + "/products",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<ProductDTO>>() { });
        return response.getBody();
    }


    public static class ParameterizedTypeReferenceImp {

    }

}
