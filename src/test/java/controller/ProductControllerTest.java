package controller;

import com.example.skip.poc4.controller.ProductController;
import com.example.skip.poc4.entities.ProductDTO;
import com.example.skip.poc4.exceptions.DBException;
import com.example.skip.poc4.exceptions.ErrorEnum;
import com.example.skip.poc4.exceptions.ErrorWrapper;
import com.example.skip.poc4.services.ProductService;
import com.example.skip.poc4.services.ProductServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

public class ProductControllerTest {

    ProductService productServiceMock;
    ProductController productController;

    private ProductDTO product1;
    private long product1Id;
    private String product1Name;
    private List<ProductDTO> products;

    @Before
    public void setup() {
        productServiceMock = mock(ProductServiceImpl.class);
        productController = new ProductController(productServiceMock);

        product1Id = 1L;
        product1Name = "Phone";
        product1 = new ProductDTO();
        product1.setName(product1Name);

        products = Arrays.asList(product1);
    }

    @Test
    public void getAll() {
        doReturn(products).when(productServiceMock).getAll();

        ResponseEntity responseEntity = productController.getAll();

        List<ProductDTO> returnedProducts = (List<ProductDTO>) responseEntity.getBody();

        assertEquals(product1Name, returnedProducts.get(0).getName());
        assertEquals(HttpStatus.OK.value(), responseEntity.getStatusCode().value());


    }

    @Test
    public void getById() throws DBException {
        doReturn(product1).when(productServiceMock).getById(product1Id);

        ResponseEntity responseEntity = productController.getById(product1Id);

        ProductDTO returnedProduct = (ProductDTO) responseEntity.getBody();

        assertEquals(product1Name, returnedProduct.getName());
        assertEquals(HttpStatus.OK.value(), responseEntity.getStatusCode().value());
    }

    @Test
    public void getByIdFail() throws DBException {
        DBException dBException = mock(DBException.class);

        doThrow(dBException).when(productServiceMock).getById(product1Id);

        ResponseEntity responseEntity = productController.getById(product1Id);

        ErrorWrapper errorWrapper = (ErrorWrapper) responseEntity.getBody();

        assertEquals(ErrorEnum.REGISTRY_NOT_FOUND_ERROR.getMsg(), errorWrapper.getMessage());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), responseEntity.getStatusCode().value());


    }
}
